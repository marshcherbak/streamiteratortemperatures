
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

public class StreamTemperatures extends Temperatures {
    public StreamTemperatures(Iterable<Temperature> temperatures) {
        super(temperatures);
    }

    public StreamTemperatures(File csv) {
        super(csv);
    }

    public StreamTemperatures(URL url) {
        super(url);
    }

    @Override
    public long size() {
        return this.stream().collect(Collectors.toList()).size();
    }

    @Override
    public List<Date> dates() {
        return stream().parallel().map(t -> t.getDate()).distinct().sorted().collect(Collectors.toList());
    }

    @Override
    public Set<String> cities() {
        Set<String> cities = stream().parallel().map(t -> t.getCity()).collect(Collectors.toSet());
        return cities;
    }

    @Override
    public Set<String> countries() {
        Set<String> countries = stream().parallel().map(t -> t.getCountry()).collect(Collectors.toSet());
        return countries;
    }

    @Override
    public Map<String, Temperatures> temperaturesByCountry() {
        Map<String, List<Temperature>> temperaturesMap = stream().collect(groupingBy(t->t.getCountry()));
        Map<String, Temperatures> tMap = new TreeMap<>();
        temperaturesMap.entrySet().stream().forEach(e -> tMap.put(e.getKey(), new StreamTemperatures(e.getValue())));
        return tMap;
    }

    @Override
    public String coldestCountryAbs() {
        Temperature temp = stream().collect(Collectors.toList()).get(0);
        //using reduce to get the smallest temperature
        Temperature t = stream().reduce(temp, (ta, tempt)->{
            if(ta.getAverageTemperature() < tempt.getAverageTemperature())
                return ta;
            else
                return tempt;
        });
        return t.getCountry();
    }

    @Override
    public String hottestCountryAbs() {
        Temperature temp = stream().collect(Collectors.toList()).get(0);
        Temperature t = stream().reduce(temp, (ta, tempt)->{
            if(ta.getAverageTemperature() > tempt.getAverageTemperature())
                return ta;
            else
                return tempt;
        });
        return t.getCountry();
    }

    @Override
    public String coldestCountryAvg() {
        Map<String, Double> tempMap = this.countriesAvgTemperature();
        Map.Entry<String, Double> first = null;
        String s = tempMap.entrySet().stream().reduce(first, (entryfirst, entry1)->{
            if(entryfirst==null || entry1.getValue()<entryfirst.getValue())
                return entry1;
            else
                return entryfirst;
        }).getKey();
        return s;
    }

    @Override
    public String hottestCountryAvg() {
        Map<String, Double> tempMap = this.countriesAvgTemperature();
        Map.Entry<String, Double> first = null;
        String s = tempMap.entrySet().stream().reduce(first, (entryfirst, entry1)->{
            if(entryfirst==null || entry1.getValue() > entryfirst.getValue())
                return entry1;
            else
                return entryfirst;
        }).getKey();
        return s;
    }

    @Override
    public Map<String, Double> countriesAvgTemperature() {
        //map with lists with groupingby
        Map<String, List<Temperature>> temperaturesMap = stream().collect(groupingBy(t->t.getCountry()));
        Map<String, Double> tMap = new TreeMap<>();
        //get average
        temperaturesMap.entrySet().stream().forEach(e -> tMap.put(e.getKey(), e.getValue().stream().parallel()
                .map(t->t.getAverageTemperature()).reduce(0., (a,b)->a+b)/e.getValue().size()));
        return tMap;
    }

    public Map<String, Double> avgTemperatureDeltaPerYearPerCountry(){
        //map with countries + temps
        Map<String, List<Temperature>> countryCities = stream().collect(groupingBy(t->t.getCountry()));

        //map with countries + cities
        Map<String, List<String>> countryCity = new TreeMap<>();
        countryCities.entrySet().stream().forEach(e ->
                countryCity.put(e.getKey(), e.getValue().stream().parallel().map(t->t.getCity()).distinct().collect(toList())));

        //map with cities + temperatures
        Map<String, List<Temperature>> citiesTemp = stream().collect(groupingBy(t->t.getCity()));

        //map with cities and calculated delta
        Map<String, Double> cityDelta = new TreeMap<>();
        citiesTemp.entrySet().stream().forEach(e->
        {
            try {
                cityDelta.put(e.getKey(), deltaCalc(e.getValue()));
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
        });

        //map with countries and calculated deltas
        Map<String, Double> finalMap = new TreeMap<>();
        countryCity.entrySet().stream().forEach(e->
                finalMap.put(e.getKey(), countryDeltaCalc(e.getValue(), cityDelta)));

        //calculate global
        List<Double> allDeltas = new ArrayList<>();
        finalMap.entrySet().stream().forEach(e-> allDeltas.add(e.getValue()));
        Double finalDelta = allDeltas.stream().parallel().mapToDouble(d->d).average().getAsDouble();
        finalMap.put("Globally", finalDelta);
        return finalMap;
    }

    //calculate average delta for each country through its cities
    private Double countryDeltaCalc(List<String> cities, Map<String, Double> cityDeltas){
        final Double[] sum = {0.};
        //gets cities in country
        //looks for key city in map cityDeltas and adds it
        cities.stream().parallel().forEach(s->{
            sum[0] += cityDeltas.get(s);
        });
        //average
        return sum[0]/cities.size();
    }

    //calculates delta for each city
    private Double deltaCalc(List<Temperature> value) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = "1900-01-01";
        final Date startDate;
        startDate = dateFormat.parse(dateString);
        //get a list off all temperatures after 1900 and sort it by date
        List<Temperature> temp = value.stream().filter(t->(t.getDate().compareTo(startDate))>=0).sorted(Comparator.comparing(t->t.getDate())).collect(toList());

        //map with years and a list of temperatures
        Map<Integer, List<Temperature>> yearTemps = temp.stream().collect(groupingBy(t->t.getDate().getYear()+1900));

        //get average for each year
        //remains sorted since using treemap
        Map<Integer, Double> yearAvg = new TreeMap<>();
        yearTemps.entrySet().stream().forEach(e -> yearAvg.put(e.getKey(), e.getValue().stream().parallel()
                .map(t->t.getAverageTemperature()).reduce(0., (a,b)->a+b)/e.getValue().size()));

        //get a list of all average temps
        //since map was sorted, the list is also sorted by years ascending
        List<Double> temperatures = new LinkedList<>();
        yearAvg.entrySet().stream().forEach(e->temperatures.add(e.getValue()));

        //stream of indexes
        Stream<Integer> helper = Stream.iterate(0, n->n+1).limit(temperatures.size()-1);

        //list of all deltas for yeach year
        List<Double> helperD = helper.parallel().map(n->temperatures.get(n+1)-temperatures.get(n)).collect(toList());

        //get average delta
        Double mainDelta = helperD.stream().parallel().mapToDouble(t->t).average().getAsDouble();
        return mainDelta;
    }


    public static void main(String[] args) {
        String path = null;
        if(args!=null && args.length!=0)
            path = args[0];
        try{
            URL url = new URL(path);
            StreamTemperatures t = new StreamTemperatures(url);
            t.printSummary();
            System.out.println();
            Map<String, Double> m = t.avgTemperatureDeltaPerYearPerCountry();
            m.forEach((key,value)-> System.out.println("Country: " + key + " || AverageDelta: "+ value));
        }
        catch (MalformedURLException e){
            //if not url trying to open a file
            System.out.println(e);
            try{
                File file = new File(path);
                StreamTemperatures t = new StreamTemperatures(file);
                double start = System.currentTimeMillis()/1000.;
                t.printSummary();
                String summary = "Summary method: " + (System.currentTimeMillis()/1000.-start);
                System.out.println("\nDeltas:");
                start = System.currentTimeMillis()/1000.;
                Map<String, Double> m = t.avgTemperatureDeltaPerYearPerCountry();
                String delta = "avgTemperatureDeltaPerYearPerCountry method: " + (System.currentTimeMillis()/1000.-start);
                m.forEach((key,value)-> System.out.println("Country: " + key + " || AverageDelta: "+ value));

                System.out.println(summary);
                System.out.println(delta);
            }
            catch (Exception ex){
                System.out.println(ex);
            }
        }
        catch (Exception e){
            System.out.println(e);
        }

    }
}

