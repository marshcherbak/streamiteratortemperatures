
import java.util.Date;

public class Temperature {
    final private Date date;
    final private double averageTemperature;
    final private double averageTemperatureUncertainty;
    final private String city;
    final private String country;
    final private double latitude;
    final private double longitude;

    public Date getDate() {
        return date;
    }

    public double getAverageTemperature() {
        return averageTemperature;
    }

    public double getAverageTemperatureUncertainty() {
        return averageTemperatureUncertainty;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public Temperature(Date date, double averageTemperature, double averageTemperatureUncertainty, String city, String country, double latitude, double longitude) {
        this.date = date;
        this.averageTemperature = averageTemperature;
        this.averageTemperatureUncertainty = averageTemperatureUncertainty;
        this.city = city;
        this.country = country;
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
