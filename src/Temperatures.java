
import java.io.*;
import java.net.Socket;
import java.util.stream.Collectors;
import java.util.*;
import java.text.SimpleDateFormat;
import java.util.function.Function;
import java.text.DateFormat;
import java.util.stream.Stream;
import java.util.Date;

public abstract class Temperatures implements Iterable<Temperature>{

    private List<Temperature> temps;
    public Temperatures(Iterable<Temperature> temperatures){
        temps = new ArrayList<>();
        for(Temperature t : temperatures)
            temps.add(t);

    }

    public Temperatures(File csv){
        List<Temperature> temp = new ArrayList<Temperature>();
        try {
            InputStream inputStream = new FileInputStream(csv);
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            temp = reader.lines() // iterate over the csv line by line (stream of Strings)
                    .skip(1) // skip header of csv file
                    .map(mapLineToTemperature) // convert line to DateTemperature
                    .filter(it -> it != null) // filter null values due to parsing errors
                    .collect(Collectors.toList());

            reader.close();
            temps = temp;
        } catch (IOException e) {
            System.out.println("File " + csv + " not found.");
        }
    }
    static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    static private Function<String, Temperature> mapLineToTemperature = (line) -> {
        String[] fields = line.split(",");// a CSV with semicolon separated lines
        try {
            Date date = dateFormat.parse(fields[0]);
            double averageTemperature = Double.parseDouble(fields[1]);
            double averageTemperatureUncertainty = Double.parseDouble(fields[2]);
            String city = fields[3];
            String country = fields[4];
            String latitudeS = fields[5].substring(0, fields[5].length()-2);
            String longitudeS = fields[6].substring(0, fields[6].length()-2);
            Double latitude = Double.parseDouble(latitudeS);
            Double longitude = Double.parseDouble(longitudeS);
            return new Temperature(date, averageTemperature, averageTemperatureUncertainty, city, country, latitude, longitude);
        } catch (Exception e) {
            return null;
        }
    };


    public Temperatures(java.net.URL url){
        List<Temperature> temp = new ArrayList<Temperature>();
        try {
            Socket socket = new Socket(url.getHost(), url.getDefaultPort());

            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
            String requestLine = "GET "+url.getPath()+" HTTP/1.1";
            String hostLine = "Host: " + url.getHost();

            out.print(requestLine + "\r\n" + hostLine + "\r\n\r\n");
            out.flush();

            temp = in.lines()/*.dropWhile(l -> !l.equals(""))*/
                    .skip(1)
                    .map(mapLineToTemperature)
                    .filter(it -> it != null) // filter null values due to parsing errors
                    .collect(Collectors.toList());
            in.close();
            temps = temp;
            socket.close();

        }
        catch(Exception e){}
    }


    public Stream<Temperature> stream(){
        return temps.stream();
    }

    //----------------------------------------------------

    public abstract long size();
    //für die Anzahl der gemessenen Temperaturen

    public abstract List<Date> dates();
    //für die Liste aller aufsteigend sortierten Tage ohne Duplikate von allen gemessenen Temperaturen,

    public abstract Set<String> cities();
    //für die Menge aller Städte von allen gemessenen Temperaturen,

    public abstract Set<String> countries();
    //für die Menge aller Länder von allen gemessenen Temperaturen,

    public abstract Map<String, Temperatures> temperaturesByCountry();
    //für die Menge aller gemessenen Temperaturen gruppiert nach Ländernamen,

    public abstract String coldestCountryAbs();
    //für das Land mit der kältesten gemessenen Temperatur,

    public abstract String hottestCountryAbs();
    //für das Land mit der heißesten gemessenen Temperatur,

    public abstract String coldestCountryAvg();
    //für das Land mit der kältesten durchschnittlichen Temperatur,

    public abstract String hottestCountryAvg();
    //für das Land mit der heißesten durchschnittlichen Temperatur,

    public abstract Map<String, Double> countriesAvgTemperature();
    //für Ländernamen plus deren dazugehörigen Durchschnittstemperatur über alle
    //Zeiten und beinhaltenden Städte.

    @Override
    public Iterator<Temperature> iterator() {
        return new TemperatureIterator(this.temps);
    }


    class TemperatureIterator implements Iterator<Temperature>{

        private List<Temperature> temperatures;
        private int count = 0;

        public TemperatureIterator(List<Temperature> temperatures){
            this.temperatures = temperatures;
        }
        @Override
        public boolean hasNext() {
            return count < this.temperatures.size();
        }

        @Override
        public Temperature next() {
            return this.temperatures.get(count++);
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }

    }

    public void printSummary(){
        System.out.println("Size: " + this.size());

        System.out.println("\nCities:");
        Set<String> cities = cities();
        System.out.println(cities);
        System.out.println("\nCountries:");
        Set<String> countries = countries();
        for(String c : countries)
            System.out.println(c);

        String coldestAbs = coldestCountryAbs();
        String hotAbs = hottestCountryAbs();
        String coldAvg = coldestCountryAvg();
        String hotAvg = hottestCountryAvg();

        System.out.println();
        System.out.println("Coldest and hottest:");
        System.out.println("Coldest country by absolute: " + coldestAbs);
        System.out.println("Hottest country by absolute: " + hotAbs);
        System.out.println("Coldest country by average: " + coldAvg);
        System.out.println("Hottest country by average: " + hotAvg + "\n");

        System.out.println("Averages:");
        Map<String, Double> averages = countriesAvgTemperature();
        for(Map.Entry<String, Double> e : averages.entrySet())
            System.out.println("Country: " + e.getKey() + "  ||  Average temperature: " + e.getValue());

    }
}
