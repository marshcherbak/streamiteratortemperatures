
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.Date;

public class IteratorTemperatures extends Temperatures {
    //some arguments so that we only need to calculate them once - saves a lot of time
    private Set<String> countriesSet = null;
    private Map<String, Temperatures> temperaturesByCountry = null;
    private Map<String, Double> avgByCountry = null;

    public IteratorTemperatures(Iterable<Temperature> temperatures) {
        super(temperatures);
    }

    public IteratorTemperatures(File csv) {
        super(csv);
    }

    public IteratorTemperatures(URL url) {
        super(url);
    }

    @Override
    public long size() {
        int i = 0;
        for(Temperature t : this)
            i++;
        return i;
    }

    @Override
    public List<Date> dates() {
        //set is by default with distinct values
        Set<Date> dates = new HashSet<>();
        for(Temperature t : this){
            dates.add(t.getDate());
        }
        List<Date> dateslist = new ArrayList<>(dates);
        Collections.sort(dateslist);
        return dateslist;
    }

    @Override
    public Set<String> cities() {
        Set<String> cities = new HashSet<>();
        for(Temperature t : this) {
            cities.add(t.getCity());
        }
        return cities;
    }

    @Override
    public Set<String> countries() {
        Set<String> countries = new HashSet<>();
        for(Temperature t : this) {
            countries.add(t.getCountry());
        }
        countriesSet = countries;
        return countries;
    }

    @Override
    public Map<String, Temperatures> temperaturesByCountry() {
        Set<String> countries;
        if(countriesSet==null)
          countries = this.countries();
        else
            countries = countriesSet;

        IteratorTemperatures ts = new IteratorTemperatures(this);
        Map<String, Temperatures> tByCountry = new TreeMap<>();

        for(String s : countries){ //for each country
            List<Temperature> list = new ArrayList<>();
            for(Temperature t : ts){
                if(t.getCountry().equals(s)) //make a list of temps
                    list.add(t);
            }
            IteratorTemperatures temperatures = new IteratorTemperatures(list);
            tByCountry.put(s, temperatures);
        }
        temperaturesByCountry = tByCountry;
        return tByCountry;
    }

    @Override
    public String coldestCountryAbs() {
        Map<String, Temperatures> tByCountry;
        if(temperaturesByCountry==null)
            tByCountry = this.temperaturesByCountry();
        else
            tByCountry = temperaturesByCountry;

        double coldest = iterator().next().getAverageTemperature();

        String coldestCountry = iterator().next().getCountry();
        for(Map.Entry<String, Temperatures> entry: tByCountry.entrySet()){
            for(Temperature t : entry.getValue()){
                if(t.getAverageTemperature() < coldest){
                    coldest = t.getAverageTemperature();
                    coldestCountry = entry.getKey();
                }
            }
        }
        return coldestCountry;
    }

    @Override
    public String hottestCountryAbs() {
        Map<String, Temperatures> tByCountry;
        if(temperaturesByCountry==null)
            tByCountry = this.temperaturesByCountry();
        else
            tByCountry = temperaturesByCountry;
        double hottest = iterator().next().getAverageTemperature();
        String hottestCountry = iterator().next().getCountry();
        for(Map.Entry<String, Temperatures> entry: tByCountry.entrySet()){
            for(Temperature t : entry.getValue()){
                if(t.getAverageTemperature() > hottest){
                    hottest = t.getAverageTemperature();
                    hottestCountry = entry.getKey();
                }
            }
        }
        return hottestCountry;
    }

    @Override
    public String coldestCountryAvg() {
        Map<String, Double> averages;

        if(avgByCountry==null)
            averages = this.countriesAvgTemperature();
        else
            averages = avgByCountry;

        Map.Entry<String, Double> first = ((TreeMap<String, Double>) averages).firstEntry();

        double fAverage = first.getValue();
        String country = first.getKey();

        for(Map.Entry<String, Double> entry : averages.entrySet()){
            if(entry.getValue()<fAverage){
                fAverage = entry.getValue();
                country = entry.getKey();
            }
        }
        return country;
    }

    @Override
    public String hottestCountryAvg() {
        Map<String, Double> averages;

        if(avgByCountry==null)
            averages = this.countriesAvgTemperature();
        else
            averages = avgByCountry;
        Map.Entry<String, Double> first = ((TreeMap<String, Double>) averages).firstEntry();
        double fAverage = first.getValue();
        String country = first.getKey();
        for(Map.Entry<String, Double> entry : averages.entrySet()){
            if(entry.getValue()>fAverage){
                fAverage = entry.getValue();
                country = entry.getKey();
            }
        }

        return country;
    }

    @Override
    public Map<String, Double> countriesAvgTemperature() {
        if(avgByCountry!=null)
            return avgByCountry;
        Map<String, Temperatures> tByCountry;
        if(temperaturesByCountry==null)
            tByCountry = this.temperaturesByCountry();
        else
            tByCountry = temperaturesByCountry;
        Map<String, Double> averages = new TreeMap<>();

        for(Map.Entry<String, Temperatures> entry: tByCountry.entrySet()){
            Temperatures itemps = entry.getValue();
            double average=0;
            for(Temperature t: itemps){
                average+=t.getAverageTemperature();
            }
            long n = itemps.size();
            average = average/n;
            averages.put(entry.getKey(), average);
        }
        avgByCountry = averages;
        return averages;
    }



    public static void main(String[] args) {
        String path;
        if(args==null || args.length==0)
            path = null;
        else
            path = args[0];
        try{
            URL url = new URL(path);
            IteratorTemperatures t = new IteratorTemperatures(url);
            t.printSummary();
        }
        catch (MalformedURLException e){
            System.out.println(e);
            try{
                File file = new File(path);
                IteratorTemperatures t = new IteratorTemperatures(file);

                t.printSummary();
            }
            catch (Exception ex){
                System.out.println(ex);
            }
        }
        catch (Exception e){
            System.out.println(e);
        }
    }
}
